using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
namespace Misc
{
    public class GameManager : MonoBehaviour
    {
        public string levelTitle = "Level Title";
        public List<Mission> missions;
        public int currentMission = 0;
        public int numberOfCurrentGoals;
        public int goalsFound = 0;
        public UnityEvent onFinishAllMissions;
        public UnityEvent onFail;
        public TMP_Text levelTitleUI;
        public TMP_Text missionTitleUI;
        public TMP_Text missionTextUI;
        public TMP_Text progressUI;
        
        private void Start()
        {
            Time.timeScale = 1f;
            foreach (Mission mission in missions)
            {
                foreach (Interactable.Interactable interactable in mission.targets)
                {
                    interactable.gameObject.SetActive(false);
                }
            }
            StartMission();
            levelTitleUI.text = levelTitle;
        }

        public void AddGoal()
        {
            goalsFound += 1;
            CheckMissionStatus();
        }

        public void SetProgressText()
        {
            progressUI.text = missions[currentMission].progressPrefix + ": " + goalsFound + "/"  + numberOfCurrentGoals;
        }

        private void CheckMissionStatus()
        {
            if (numberOfCurrentGoals == goalsFound)
            {
                missions[currentMission].onFinish.Invoke();
                currentMission += 1;
                Debug.Log("Mission " + currentMission + " Finished!");
                if (currentMission < missions.Count)
                {
                    StartMission();
                }
                else
                {
                    EndLevel();
                }
            }
            else
            {
                SetProgressText();
            }
        }

        private void StartMission()
        {
            Debug.Log("Mission Started!");
            missions[currentMission].onStart.Invoke();
            missionTextUI.text = missions[currentMission].shortDescription;
            missionTitleUI.text = missions[currentMission].title;
            numberOfCurrentGoals = missions[currentMission].targets.Count;
            goalsFound = 0;
            foreach (Interactable.Interactable interactable in missions[currentMission].targets)
            {
                interactable.gameObject.SetActive(true);
            }
            SetProgressText();
        }

        private void EndLevel()
        {
            Debug.Log("Level Finished!");
            onFinishAllMissions.Invoke();
        }
        
        public void LevelFailed()
        {
            Debug.Log("Level Failed!");
            onFail.Invoke();
        }

        public void SetTimeScale(float scale)
        {
            Time.timeScale = scale;
        }
    }
}

[Serializable]
public class Mission
{
    public string title = "Mission Title";
    public string shortDescription = "Mission Title";
    public string progressPrefix = "Progress Prefix";
    public List<Interactable.Interactable> targets;
    public UnityEvent onFinish;
    public UnityEvent onStart;
}