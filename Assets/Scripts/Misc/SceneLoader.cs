using UnityEngine;
using UnityEngine.SceneManagement;

namespace Misc
{
    public class SceneLoader : MonoBehaviour
    {
        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
        
        public void Retry()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        
        public void Exit()
        {
            Application.Quit();
        }
    }
}