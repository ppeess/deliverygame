using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Misc
{
    public class InteractionManager : MonoBehaviour
    {
        public GameObject interactMenu;
        public TMP_Text interactMenuDescription;
        public float currentInteractionPriority = -1;
        public UnityEvent interact;

        public bool SetInteractableUI(string input, float priority, UnityAction interaction)
        {
            if (priority > currentInteractionPriority)
            {
                currentInteractionPriority = priority;
                interactMenu.SetActive(true);
                interactMenuDescription.text = "[E] " + input;
                interact.AddListener(interaction);
                return true;
            }

            return false;
        }

        public void EndInteraction()
        {
            if (interactMenu == null)
            {
                return;
            }
            interactMenu.SetActive(false);
            interact.RemoveAllListeners();
            currentInteractionPriority = -1;
        }

        
        public void Interact(InputAction.CallbackContext context) {
            if (context.performed) {
                Debug.Log("Interact Pressed!");
                interact.Invoke();
            }
        }
    }
}