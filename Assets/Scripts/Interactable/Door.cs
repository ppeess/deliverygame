using System;
using System.Collections;
using UnityEngine;

namespace Interactable
{
    public class Door : MonoBehaviour
    {
        [SerializeField] private Vector3 openPosition;
        [SerializeField] private float openTime;
        [SerializeField] private AnimationCurve transition;
        
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, transform.position + openPosition);
        }

        public void Open()
        {
            StartCoroutine(Transition());
        }

        private IEnumerator Transition()
        {
            float currentTime = 0f;
            while (currentTime < openTime)
            {
                float curveValue = transition.Evaluate(Mathf.Max(0, 1 - (currentTime / openTime)));
                transform.position = (1 - curveValue) * openPosition + transform.position;
                currentTime += Time.deltaTime;
                yield return null;
            }
        }
    }
}