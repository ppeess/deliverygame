using System;
using Misc;
using UnityEngine;
using UnityEngine.Events;

namespace Interactable
{
    public class Interactable : MonoBehaviour
    {
        [SerializeField] private string actionName = "Collect";
        [SerializeField] private float interactRadius;
        [SerializeField] private int prio = 1;
        [SerializeField] private bool interacting;
        [SerializeField] public UnityEvent interaction;
        private InteractionManager _interactionManager;
        
        private void Start()
        {
            _interactionManager = GameObject.Find("InteractionManager").GetComponent<InteractionManager>();
            if (_interactionManager == null)
            {
                Debug.LogError("ADD INTERACTION MANAGER");
            }
        }

        public void Update()
        {
            Collider[] colliders =
                Physics.OverlapSphere(transform.position, interactRadius, LayerMask.GetMask("Player"));
            
            if (!interacting && colliders.Length > 0)
            {
                if (_interactionManager.SetInteractableUI(actionName, prio, () => interaction.Invoke()))
                {
                    interacting = true;
                }
            }
            else if (interacting && colliders.Length == 0)
            {
                interacting = false;
                _interactionManager.EndInteraction();
            }
        }

        private void OnDisable()
        {
            if (interacting)
            {
                interacting = false;
                _interactionManager.EndInteraction();
            }
        }

        private void OnDestroy()
        {
            if (interacting)
            {
                interacting = false;
                _interactionManager.EndInteraction();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, interactRadius);
        }
    }
}