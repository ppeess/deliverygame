using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace DialogueSystem
{
    public class DialogueInstance : MonoBehaviour
    {
        [SerializeField] private GameObject dialogue;
        [SerializeField] private TMP_Text dialogueText;
        [SerializeField] private TMP_Text dialogueSpeaker;
        public UnityEvent onExitDialogue;
        public List<DialogueEntry> dialogueEntries;
        public int current = 0;
        public bool inDialogue = false;

        public void StartDialogue()
        {
            if (inDialogue)
            {
                return;
            }

            inDialogue = true;
            current = 0;
            dialogue.SetActive(true);
            StartCoroutine(ProcessDialogueEntry());
        }

        public IEnumerator ProcessDialogueEntry()
        {
            SetDialogueEntry();
            yield return new WaitForSeconds(dialogueEntries[current].duration);
            NextEntry();
        }

        public void NextEntry()
        {
            current += 1;
            if (current < dialogueEntries.Count)
            {
                StartCoroutine(ProcessDialogueEntry());
            }
            else
            {
                dialogue.SetActive(false);
                onExitDialogue.Invoke();
            }
        }

        public void SetDialogueEntry()
        {
            dialogueText.text = dialogueEntries[current].text;
            dialogueSpeaker.text = dialogueEntries[current].speaker;
        }
    }

    [Serializable]
    public struct DialogueEntry
    {
        public string speaker;
        public string text;
        public float duration;
    }
}