using UnityEngine;

namespace AI.Sensors
{
    public class SensorInput : MonoBehaviour
    {
        [SerializeField]
        private float emissionValue;
        [SerializeField]
        private SensorType sensorType;
        [SerializeField]
        private Priority sensePriority;

        public float GetEmissionValue()
        {
            return emissionValue;
        }

        public Priority GetSensePriority()
        {
            return sensePriority;
        }
        
        public SensorType GetSensorType()
        {
            return sensorType;
        }
    
        public void SetEmissionValue(float newValue)
        {
            emissionValue = newValue;
        }
        
        public void SetSensorType(SensorType newValue)
        {
            sensorType = newValue;
        }
        
        
        public void SetSensePriority(Priority newValue)
        {
            sensePriority = newValue;
        }
    }
}
