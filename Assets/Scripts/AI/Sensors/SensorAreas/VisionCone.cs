using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace AI.Sensors.SensorAreas
{
    public class VisionCone : SensorArea
    {
        [SerializeField] private Transform viewDirection;
        [SerializeField, Range(0, 360)] private float viewAngle;
        [SerializeField, Range(0, 1)] private float meshResolution;
        [SerializeField] private int edgeResolveIterations;
        [SerializeField] private float edgeDstThreshold;

        private Mesh viewMesh;
        [SerializeField] private MeshFilter viewMeshFilter;
        [SerializeField] protected LayerMask visionConeLayerMask;
        
        public new void Start()
        {
            base.Start();
            
            viewMesh = new Mesh ();
            viewMesh.name = "View Mesh";
            viewMeshFilter.mesh = viewMesh;
        }
        
        public new void Update()
        {
            base.Update();

            List<SensorInputInstance> toRemove = new List<SensorInputInstance>();
            Vector3 position = transform.position;

            foreach (SensorInputInstance sensorInputInstance in sensedTargets)
            {
                RaycastHit hit;
                if (Physics.Raycast(position + offset,
                        sensorInputInstance.GetSensorInput().transform.position - position + offset, out hit, radius))
                {
                    Debug.DrawRay(position + offset, hit.transform.position - position + offset);
                    if ((sensorInputInstance.GetSensorInput().transform == hit.collider.transform || sensorInputInstance
                            .GetSensorInput().transform.IsChildOf(hit.collider.transform)))
                    {
                        Vector3 direction = Quaternion.Euler(viewDirection.rotation.eulerAngles) *
                            viewDirection.localPosition * radius;
                        Vector3 origin = hit.transform.position - position + offset;
                        Debug.DrawRay(position + offset, direction, Color.blue);
                        float angle = Mathf.Abs(Vector2.Angle(
                            new Vector2(direction.x, direction.z).normalized, new Vector2(origin.x, origin.z)));
                        
                        if (angle >= viewAngle * 0.5f)
                        {
                            toRemove.Add(sensorInputInstance);
                        }
                    }
                    else
                    {
                        toRemove.Add(sensorInputInstance);
                    }
                }
                else
                {                            
                    toRemove.Add(sensorInputInstance);
                    Debug.DrawRay(position + offset, sensorInputInstance.GetSensorInput().transform.position - position + offset);
                }
            }
            
            foreach (SensorInputInstance sensorInputInstance in toRemove)
            {
                sensedTargets.Remove(sensorInputInstance);
            }
        }
        
        private void LateUpdate() {
            DrawFieldOfView ();
        }

        private new void OnDrawGizmosSelected()
        {
            base.OnDrawGizmosSelected();
            Vector3 position = transform.position;
            Quaternion rotation = viewDirection.rotation;
            Vector3 localPosition = viewDirection.localPosition;
            Gizmos.DrawLine(position + offset, position + offset + Quaternion.Euler(rotation.eulerAngles) * localPosition * radius);
            Gizmos.DrawLine(position + offset, position + offset + Quaternion.Euler(rotation.eulerAngles + new Vector3(0, viewAngle * 0.5f, 0)) * localPosition * radius);
            Gizmos.DrawLine(position + offset, position + offset + Quaternion.Euler(rotation.eulerAngles - new Vector3(0, viewAngle * 0.5f, 0)) * localPosition * radius);
        }

        private void DrawFieldOfView()
        {
            int steps = Mathf.RoundToInt(viewAngle * meshResolution);
            float stepAngleSize = viewAngle / steps;
            List<Vector3> viewPoints = new List<Vector3>();
            ViewRayInfo oldViewRay = new ViewRayInfo();

            for (int i = 0; i <= steps; i++)
            {
                float angle = - viewAngle / 2 + stepAngleSize * i;
                ViewRayInfo newViewRay = ViewRay(angle);
                
                if (i > 0) {
                    bool edgeDstThresholdExceeded = Mathf.Abs (oldViewRay.distance - newViewRay.distance) > edgeDstThreshold;
                    if (oldViewRay.hit != newViewRay.hit || (oldViewRay.hit && newViewRay.hit && edgeDstThresholdExceeded)) {
                        EdgeInfo edge = FindEdge (oldViewRay, newViewRay);
                        if (edge.pointA != Vector3.zero) {
                            viewPoints.Add (edge.pointA);
                        }
                        if (edge.pointB != Vector3.zero) {
                            viewPoints.Add (edge.pointB);
                        }
                    }
                }
                
                viewPoints.Add (newViewRay.point);
                oldViewRay = newViewRay;
            }
            
            int vertexCount = viewPoints.Count + 1;
            Vector3[] vertices = new Vector3[vertexCount];
            int[] triangles = new int[(vertexCount-2) * 3];

            // for (int i = 0; i < viewPoints.Count; i++)
            // {
            //     viewPoints[i] -= offset;
            // }

            vertices [0] = Vector3.zero;
            for (int i = 0; i < vertexCount - 1; i++) {
                vertices [i + 1] = transform.InverseTransformPoint(viewPoints [i]);

                if (i < vertexCount - 2) {
                    triangles [i * 3] = 0;
                    triangles [i * 3 + 1] = i + 1;
                    triangles [i * 3 + 2] = i + 2;
                }
            }

            viewMesh.Clear ();

            viewMesh.vertices = vertices;
            viewMesh.triangles = triangles;
            viewMesh.RecalculateNormals ();
        }
        
        EdgeInfo FindEdge(ViewRayInfo minViewCast, ViewRayInfo maxViewCast) {
            float minAngle = minViewCast.angle;
            float maxAngle = maxViewCast.angle;
            Vector3 minPoint = Vector3.zero;
            Vector3 maxPoint = Vector3.zero;

            for (int i = 0; i < edgeResolveIterations; i++) {
                float angle = (minAngle + maxAngle) / 2;
                ViewRayInfo newViewCast = ViewRay (angle);

                bool edgeDstThresholdExceeded = Mathf.Abs (minViewCast.distance - newViewCast.distance) > edgeDstThreshold;
                if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded) {
                    minAngle = angle;
                    minPoint = newViewCast.point;
                } else {
                    maxAngle = angle;
                    maxPoint = newViewCast.point;
                }
            }

            return new EdgeInfo (minPoint, maxPoint);
        }
        
        private ViewRayInfo ViewRay(float angle)
        {
            RaycastHit hit;
            Vector3 position = transform.position;
            Quaternion rotation = viewDirection.rotation;
            Vector3 localPosition = viewDirection.localPosition;
            
            if (Physics.Raycast(position + offset, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, angle, 0)) *
                                                   localPosition * radius, out hit, radius, visionConeLayerMask, QueryTriggerInteraction.Ignore))
            {
                return new ViewRayInfo(true, hit.point, hit.distance, angle);
            }
            
            return new ViewRayInfo(false, position + offset + Quaternion.Euler(rotation.eulerAngles + new Vector3(0, angle, 0)) *
                localPosition.normalized * radius, radius, angle);
        }
        
        public struct ViewRayInfo
        {
            public bool hit;
            public Vector3 point;
            public float distance;
            public float angle;

            public ViewRayInfo(bool hit, Vector3 point, float distance, float angle)
            {
                this.hit = hit;
                this.point = point;
                this.distance = distance;
                this.angle = angle;
            }
        }
        
        public struct EdgeInfo {
            public Vector3 pointA;
            public Vector3 pointB;

            public EdgeInfo(Vector3 _pointA, Vector3 _pointB) {
                pointA = _pointA;
                pointB = _pointB;
            }
        }
    }
}
