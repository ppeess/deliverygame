using System.Collections.Generic;
using UnityEngine;

namespace AI.Sensors.SensorAreas
{
    public class SensorArea : MonoBehaviour
    {
        [SerializeField] protected Vector3 offset;
        [SerializeField] protected float radius;
        [SerializeField] protected SensorType sensorType;
        [SerializeField] protected Color color = Color.green;
        [SerializeField] protected List<SensorInputInstance> sensedTargets;
        [SerializeField] protected LayerMask layerMask = ~0;

        public void Start()
        {
            sensedTargets ??= new List<SensorInputInstance>();
        }

        protected void Update()
        {
            Collider[] foundColliders = Physics.OverlapSphere(transform.position + offset, radius, layerMask, QueryTriggerInteraction.Collide);
            List<SensorInputInstance> toRemove = new List<SensorInputInstance>(sensedTargets);
            
            foreach (Collider foundCollider in foundColliders)
            {
                SensorInput[] sensorInputs = foundCollider.gameObject.GetComponentsInChildren<SensorInput>();
                if (sensorInputs.Length > 0)
                {
                    foreach (SensorInput sensorInput in sensorInputs)
                    {
                        if (sensorInput.GetSensorType() == sensorType)
                        {
                            SensorInputInstance sensorInputInstance = new SensorInputInstance(sensorInput);
                            if (!sensedTargets.Contains(sensorInputInstance))
                            {
                                sensedTargets.Add(sensorInputInstance);
                            }
                            else
                            {
                                toRemove.Remove(sensorInputInstance);
                            }
                        }
                    }
                }
            }

            foreach (SensorInputInstance sensorInputInstance in toRemove)
            {
                sensedTargets.Remove(sensorInputInstance);
            }

            foreach (SensorInputInstance sensorInputInstance in sensedTargets)
            {
                sensorInputInstance.Update(Time.deltaTime);
            }
        }

        protected void OnDrawGizmosSelected()
        {
            Gizmos.color = color;
            Gizmos.DrawWireSphere(transform.position + offset, radius);
            
            foreach (SensorInputInstance sensorInputInstance in sensedTargets)
            {
                if (sensorInputInstance.GetSensorInput() != null)
                    Gizmos.DrawWireCube(sensorInputInstance.GetSensorInput().transform.position, new Vector3(0.25f, 0.25f, 0.25f));
            }
        }

        public float GetRadius()
        {
            return radius;
        }
        
        
        public List<SensorInputInstance> GetSensedTargets()
        {
            return sensedTargets;
        }

        public Vector3 GetOffset()
        {
            return offset;
        }
    }
}