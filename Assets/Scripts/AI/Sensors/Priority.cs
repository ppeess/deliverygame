using UnityEngine;

namespace AI.Sensors
{
    public enum Priority
    {
        Low = 1,
        Medium = 2,
        High = 4,
        Critical = 8,
        Unfocusable = 0
    }
}