using System;
using System.Collections.Generic;
using AI.Sensors;
using AI.Sensors.SensorAreas;
using UnityEngine;
using UnityEngine.Events;

namespace AI.Sensors
{
    public abstract class Sensor : MonoBehaviour
    {
        [SerializeField] protected float currentNoise;
        [SerializeField] protected float baseNoise;
        [SerializeField] protected float senseThreshold;
        [SerializeField] protected float senseDuration;
        
        [SerializeField] protected List<SensorInputInstanceData> currentInputs;
        [SerializeField] protected UnityEvent<SensorInputInstanceData, float> foundTarget;
        [SerializeField] protected UnityEvent<SensorInputInstanceData> lostTarget;

        [SerializeField] protected SensorArea sensorArea;
        
        private void Start()
        {
            foundTarget ??= new UnityEvent<SensorInputInstanceData, float>();
            lostTarget ??= new UnityEvent<SensorInputInstanceData>();
            currentInputs ??= new List<SensorInputInstanceData>();
            sensorArea ??= GetComponent<SensorArea>();
        }

        private void Update()
        {
            List<SensorInputInstanceData> toRemove = new List<SensorInputInstanceData>(currentInputs);
            foreach (SensorInputInstance sensorInputInstance in sensorArea.GetSensedTargets())
            {
                SensorInputInstanceData data = new SensorInputInstanceData(sensorInputInstance);
                if (!currentInputs.Contains(data))
                {
                    currentInputs.Add(data);
                }
                else
                {
                    toRemove.Remove(data);
                }
            }
            
            foreach (SensorInputInstanceData sensorInputInstance in toRemove)
            {
                currentInputs.Remove(sensorInputInstance);
                LostTarget(sensorInputInstance);
            }
            
            foreach (SensorInputInstanceData sensorInputInstance in currentInputs)
            {
                sensorInputInstance.Update(Time.deltaTime);
                if (sensorInputInstance.WasRegistered())
                {
                    EmitSignal(sensorInputInstance);
                }
                else if (TryRegister(sensorInputInstance.GetInstance()))
                {
                    sensorInputInstance.SetRegistered(true);
                    EmitSignal(sensorInputInstance);
                }
            }

            if (currentInputs.Count > 0)
            {
                CalculateNoise();
            }
        }

        public void EmitSignal(SensorInputInstanceData sensorInput)
        {            
            // Consider how recently it was registered, the value difference to current noise, priority level
            float baseValue = GetValueDistance(sensorInput) - currentNoise * senseThreshold;
            foundTarget.Invoke(sensorInput, baseValue);
        }
        
        public void LostTarget(SensorInputInstanceData sensorInput)
        {
            if (sensorInput.WasRegistered())
            {
                lostTarget.Invoke(sensorInput);
            }
        }
        
        public abstract bool TryRegister(SensorInputInstance sensorInput);
        
        public abstract void CalculateNoise();
        
        public void AddListener(UnityAction<SensorInputInstanceData, float> action)
        {
            foundTarget.AddListener(action);
        }
    
        public void RemoveListener(UnityAction<SensorInputInstanceData, float> action)
        {
            foundTarget.RemoveListener(action);
        }
        
        public void AddLostListener(UnityAction<SensorInputInstanceData> action)
        {
            lostTarget.AddListener(action);
        }
    
        public void RemoveLostListener(UnityAction<SensorInputInstanceData> action)
        {
            lostTarget.RemoveListener(action);
        }

        public void Reset()
        {
            foundTarget.RemoveAllListeners();
            lostTarget.RemoveAllListeners();
        }

        private void OnDrawGizmosSelected()
        {
            foreach (SensorInputInstanceData data in currentInputs)
            {
                if (data.WasRegistered())
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.black;
                }

                if (data.GetInstance().GetSensorInput() != null)
                {
                    Gizmos.DrawCube(data.GetInstance().GetSensorInput().transform.position, new Vector3(0.25f, 0.25f, 0.25f));
                }
            }
        }

        public abstract float GetValueDistance(SensorInputInstanceData input);
        public abstract float GetValueDistance(SensorInputInstance input);
        public abstract float GetValueDistance(SensorInput input);
    }
}

