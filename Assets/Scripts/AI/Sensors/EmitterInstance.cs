using UnityEngine;

namespace AI.Sensors
{
    [CreateAssetMenu(fileName = "EmitterInstance", menuName = "Emitters", order = 0)]
    public class EmitterInstance : ScriptableObject
    {
        [SerializeField] public SensorType sensorType;
        [SerializeField] public Priority priority;
        [SerializeField] public float emissionValue;

        [SerializeField] public float minDuration;
        [SerializeField] public float maxDuration;
        
        [SerializeField] public float minInterval;
        [SerializeField] public float maxInterval;
        [SerializeField] public int numberOfRepeats;

        [SerializeField] public GameObject baseGameObject = null;

    }
}