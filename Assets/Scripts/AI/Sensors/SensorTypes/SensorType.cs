using UnityEngine;

namespace AI
{
    public enum SensorType
    {
        Vision,
        Sound,
        Smell
    }
}