using UnityEngine;

namespace AI.Sensors.SensorTypes
{
    public class VisionSensor : Sensor
    {
        public override bool TryRegister(SensorInputInstance sensorInput)
        {
            return sensorInput.GetDuration() >= senseDuration && GetValueDistance(sensorInput) > currentNoise * senseThreshold;
        }

        public override void CalculateNoise()
        {
            if (currentInputs.Count > 0)
            {
                currentInputs.Sort(CompareValueDistance);
                currentNoise = Mathf.Max(baseNoise, GetValueDistance(currentInputs[0]));   
            }
            else
            {
                currentNoise = baseNoise;
            }
        }

        private int CompareValueDistance(SensorInputInstanceData input1, SensorInputInstanceData input2)
        {
            float value = GetValueDistance(input1) - GetValueDistance(input2);
            if (Mathf.Abs(value) > 0.02)
            {
                if (value > 0)
                {
                    return -1;
                }
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public override float GetValueDistance(SensorInputInstanceData input)
        {
            return GetValueDistance(input.GetInstance().GetSensorInput());
        }

        public override float GetValueDistance(SensorInputInstance input)
        {
            return GetValueDistance(input.GetSensorInput());
        }

        public override float GetValueDistance(SensorInput input)
        {
            return (1 - Vector3.Distance(transform.position, input.transform.position) / sensorArea.GetRadius()) *
                   input.GetEmissionValue();
        }
    }
}