using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace AI.Sensors
{
    public class SensorInputEmitter : MonoBehaviour
    {
        [FormerlySerializedAs("defaultInstance")] [SerializeField] private EmitterInstance defaultEmitter;
        private SensorType _sensorType;
        private Priority _priority;
        private float _emissionValue;
        private float _minDuration;
        private float _maxDuration;
        private float _minInterval;
        private float _maxInterval;
        private int _numberOfRepeats;
        private GameObject _baseGameObject = null;

        private void Start()
        {
            _minDuration = defaultEmitter.minDuration;
            _maxDuration = defaultEmitter.maxDuration;
            _priority = defaultEmitter.priority;
            _sensorType = defaultEmitter.sensorType;
            _emissionValue = defaultEmitter.emissionValue;
            _minInterval = defaultEmitter.minInterval;
            _maxInterval = defaultEmitter.maxInterval;
            _numberOfRepeats = defaultEmitter.numberOfRepeats;
            _baseGameObject = defaultEmitter.baseGameObject;
        }

        public void Emit()
        {
            float duration = _minDuration;
            if (_minDuration >= 0)
            {
                duration = Random.Range(_minDuration, _maxDuration);
            }
            EmitCustom(_sensorType, _priority, _emissionValue, duration, _numberOfRepeats, _minInterval, _maxInterval);
        }

        private IEnumerator EmitOverTime(SensorType sensorType, Priority priority, float emissionValue, float duration, int numberOfRepeats = 0, float minInterval = 0, float maxInterval = 0, GameObject gameObject = null)
        {
            if (gameObject == null)
            {
                gameObject = _baseGameObject;
            }
            
            if (numberOfRepeats > 0)
            {
                int remainingRepeats = numberOfRepeats;
                while (remainingRepeats > 0)
                {
                    GameObject instance = SpawnEmitter(sensorType, priority, emissionValue, gameObject);
            
                    if (duration >= 0f)
                    {
                        Destroy(instance, 0.01f + duration);
                    }
                    duration = Random.Range(_minDuration, _maxDuration);
                    remainingRepeats -= 1;
                    yield return new WaitForSeconds(Random.Range(minInterval, maxInterval));
                }
            }
            else
            {
                while (true)
                {
                    GameObject instance = SpawnEmitter(sensorType, priority, emissionValue, gameObject);
            
                    if (duration >= 0f)
                    {
                        Destroy(instance, 0.01f + duration);
                    }
                    duration = Random.Range(_minDuration, _maxDuration);
                    yield return new WaitForSeconds(Random.Range(minInterval, maxInterval));
                }
            }
            
        }
        
        public void EmitCustom(EmitterInstance emitter)
        {
            if (emitter == null)
            {
                emitter = defaultEmitter;
            }
            float minDuration = emitter.minDuration;
            float maxDuration = emitter.maxDuration;
            Priority priority = emitter.priority;
            SensorType sensorType = emitter.sensorType;
            float emissionValue = emitter.emissionValue;
            float minInterval = emitter.minInterval;
            float maxInterval = emitter.maxInterval;
            int numberOfRepeats = emitter.numberOfRepeats;
            GameObject gameObject = emitter.baseGameObject;
            float duration = Random.Range(minDuration, maxDuration);
            
            if (gameObject == null)
            {
                gameObject = _baseGameObject;
            }
            
            if (_numberOfRepeats == 0)
            {
                GameObject instance = SpawnEmitter(sensorType, priority, emissionValue, gameObject);
            
                if (duration >= 0f)
                {
                    Destroy(instance, 0.01f + duration);
                }
            }
            else
            {
                StartCoroutine(EmitOverTime(sensorType, priority, emissionValue, duration, numberOfRepeats, minInterval, maxInterval, gameObject));
            }
        }
        
        public void EmitCustom(SensorType sensorType, Priority priority, float emissionValue, float duration, int numberOfRepeats = 0, float minInterval = 0, float maxInterval = 0, GameObject gameObject = null)
        {
            if (gameObject == null)
            {
                gameObject = _baseGameObject;
            }
            
            if (numberOfRepeats == 0)
            {
                GameObject instance = SpawnEmitter(sensorType, priority, emissionValue, gameObject);
            
                if (duration >= 0f)
                {
                    Destroy(instance, 0.01f + duration);
                }
            }
            else
            {
                StartCoroutine(EmitOverTime(sensorType, priority, emissionValue, duration, numberOfRepeats, minInterval, maxInterval, gameObject));
            }
        }

        private GameObject SpawnEmitter(SensorType sensorType, Priority priority, float emissionValue, GameObject gameObject)
        {
            GameObject newObject;
            if (gameObject == null)
            {
                newObject = new GameObject();
                BoxCollider collider = newObject.AddComponent<BoxCollider>();
                collider.isTrigger = true;
            }
            else
            {
                newObject = gameObject;
            }
            
            GameObject instance = Instantiate(newObject, transform.position, Quaternion.identity);

            SensorInput input = instance.AddComponent<SensorInput>();
            input.SetEmissionValue(emissionValue);
            input.SetSensePriority(priority);
            input.SetSensorType(sensorType);

            return instance;
        }
    }
}