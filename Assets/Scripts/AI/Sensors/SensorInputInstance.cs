using UnityEngine;

namespace AI.Sensors
{
    [System.Serializable]
    public class SensorInputInstance
    {
        [SerializeField]
        private float _senseDuration = 0f;
        [SerializeField]
        private readonly SensorInput _sensorInput;

        public SensorInputInstance(SensorInput input)
        {
            _senseDuration = 0f;
            _sensorInput = input;
        }
        
        public void Update(float deltaTime)
        {
            _senseDuration += deltaTime;
        }

        public SensorInput GetSensorInput()
        {
            return _sensorInput;
        }
        
        public float GetDuration()
        {
            return _senseDuration;
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
                return false;
            SensorInputInstance inputInstance = obj as SensorInputInstance ;
            if (inputInstance == null)
                return false;
            return Equals(inputInstance);
        }

        public bool Equals(SensorInputInstance other)
        {
            if (other == null)
                return false;
            return _sensorInput == other.GetSensorInput();
        }
    }
}