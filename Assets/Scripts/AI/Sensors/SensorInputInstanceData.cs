using System;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace AI.Sensors
{
    [Serializable]
    public class SensorInputInstanceData
    {
        [SerializeField]
        private SensorInputInstance sensorInputInstance;
        [SerializeField]
        private bool wasRegistered;

        [SerializeField] private float registerDuration = 0f;

        public void Update(float deltaTime)
        {
            if (wasRegistered)
            {
                registerDuration += deltaTime;
            }
        }
        
        public SensorInputInstanceData(SensorInputInstance sensorInputInstance)
        {
            this.sensorInputInstance = sensorInputInstance; 
            wasRegistered = false;
        }

        public bool WasRegistered()
        {
            return wasRegistered;
        }
   
        public float GetRegisteredDuration()
        {
            return registerDuration;
        }
        
        public SensorInputInstance GetInstance()
        {
            return sensorInputInstance;
        }

        public void SetRegistered(bool registered)
        {
            wasRegistered = registered;
        }

        public void ResetRegisteredDuration()
        {
            registerDuration = 0f;
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            else
            {
                SensorInputInstanceData inputInstance = obj is SensorInputInstanceData data ? data : default;
                return Equals(inputInstance);    
            }
        }

        public bool Equals(SensorInputInstanceData other)
        {
            return sensorInputInstance.Equals(other.GetInstance());
        }
    }
}