using System;
using AI.Sensors;
using UnityEngine;

namespace AI
{
    [Serializable]
    public class SensingInstance
    {
        [SerializeField] private SensorInputInstanceData data;
        [SerializeField] private Vector3 location;
        [SerializeField] private bool removed;
        [SerializeField] private float timeSinceRemoval;
        [SerializeField] private float distanceValue;
        [SerializeField] private float currentPriority;
        [SerializeField] private int basePriority;
        private float interestDecayTime = 4f;
        
        public SensingInstance(SensorInputInstanceData data, float distanceValue)
        {
            this.data = data;
            this.distanceValue = distanceValue;
            basePriority = (int)data.GetInstance().GetSensorInput().GetSensePriority();
            removed = false;
        }

        public SensorInputInstanceData GetData()
        {
            return data;
        }
        
        public void SetData(SensorInputInstanceData data)
        {
            this.data = data;
        }

        public void SetDistanceValue(float newDistance)
        {
            distanceValue = newDistance;
        }

        public void Update(float deltaTime)
        {
            if (removed)
            {
                timeSinceRemoval += deltaTime;
            }
        }

        public float GetCurrentPriority()
        {
            currentPriority = Mathf.Max(basePriority / 2f,
                                  Mathf.Max(1, 1 - Mathf.Min(0, data.GetRegisteredDuration() / interestDecayTime)) *
                                  basePriority) * Mathf.Max(0.01f, distanceValue) * Mathf.Max(0, interestDecayTime - timeSinceRemoval);
            if (!removed)
            {
                currentPriority =  Mathf.Max(currentPriority, basePriority);
            }
            return currentPriority;
        }

        public Vector3 GetLocation()
        {
            if (!removed && data.GetInstance().GetSensorInput() != null)
            {
                location = data.GetInstance().GetSensorInput().transform.position;
            }
            return location;
        }

        public bool WasRemoved()
        {
            return removed;
        }
        
        public void SetRemoved(bool removed)
        {
            this.removed = removed;
            if (!removed)
            {
                data.ResetRegisteredDuration();
                timeSinceRemoval = 0;
            }
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            else
            {
                SensingInstance inputInstance = obj as SensingInstance;
                return Equals(inputInstance);    
            }
        }

        public bool Equals(SensingInstance other)
        {
            if (other == null)
                return false;
            return data.Equals(other.GetData());
        }
    }
}