using System;
using System.Collections;
using System.Collections.Generic;
using Misc;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace AI.StateSystem
{
    [Serializable]
    public class State
    {
        public string identifier;
        public List<Transition> transitions = new List<Transition>();
        public float stateDuration = 0f;
        public float exitDuration = 0f;
        public float enterDuration = 0f;

        private bool _entering;
        private bool _canExit;
        private bool _exiting;

        private float shouldExit = -1f;

        [SerializeField] private UnityEvent<State> duringState;
        [SerializeField] private UnityEvent<State> stateEnter;
        [SerializeField] private UnityEvent<State> stateExit;

        public bool GetExitStatus()
        {
            return _canExit;
        }
        
        public float GetShouldExit()
        {
            return shouldExit;
        }
        
        public void SetShouldExit(float newValue)
        {
            shouldExit = newValue;
        }
        
        public void OnStateEnter()
        {
            shouldExit = -1f;
            stateDuration = 0f;
            _entering = true;
            _canExit = false;
            GameObject.Find("CoroutineManager").GetComponent<CoroutineManager>().StartCoroutine(EnterState());
        }

        public IEnumerator ExitState()
        {
            yield return new WaitForSeconds(exitDuration);
            _exiting = false;
            _canExit = true;
        }
        
        public IEnumerator EnterState()
        {
            yield return new WaitForSeconds(enterDuration);
            _entering = false;
        }

        public void OnStateExit()
        {
            _exiting = true;            
            _canExit = false;
            GameObject.Find("CoroutineManager").GetComponent<CoroutineManager>().StartCoroutine(ExitState());
        }

        public void DuringState(float deltaTime)
        {
            stateDuration += deltaTime;
            if (_entering)
            {
                stateEnter.Invoke(this);
            }
            else if (_exiting)
            {
                stateExit.Invoke(this);
            }
            else if (!_canExit)
            {
                duringState.Invoke(this);
            }
        }

        public string CheckTransitions()
        {
            if (_entering)
            {
                return "";
            }

            List<string> nextStateCandidates = new List<string>();
            string nextStateIdentifier = "";
            int currentPriority = -1;
            
            foreach (Transition transition in transitions)
            {
                if (transition.CanTransition())
                {
                    if (currentPriority < transition.GetPriority())
                    {
                        currentPriority = transition.GetPriority();
                        nextStateCandidates = new List<string>();
                        nextStateCandidates.Add(transition.GetNextState());
                    }
                    else
                    {
                        nextStateCandidates.Add(transition.GetNextState());
                    }
                }
            }

            if (nextStateCandidates.Count > 0)
            {
                nextStateIdentifier = nextStateCandidates[Random.Range(0, nextStateCandidates.Count)];
            }
            
            return nextStateIdentifier;
        }
    }
}
