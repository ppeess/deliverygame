using System;
using System.Collections.Generic;
using Misc;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace AI.StateSystem
{
    public class AIActions : MonoBehaviour
    {
        [SerializeField] private AISensing sensing;
        [SerializeField] private NavMeshAgent agent;
        
        [SerializeField] private bool canNavigate = false;
        [SerializeField] private bool isGuard = false;
        

        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private bool isChasing = false;
        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private bool wasChasingBefore = false;
        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private float chaseSpeed = 1.5f;
        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private SensingInstance chaseTarget;
        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private float maxChaseDistance = -1f;
        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private Vector3 chaseStartPosition;

        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private float searchRadius = 15f;        
        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private bool isSearching = false;
        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private Vector3 searchDestination;
        
        [ShowIf("@this.canNavigate && !this.isGuard"), SerializeField] private float wanderSpeed = 0.75f;

        [ShowIf("@this.canNavigate && this.isGuard"), SerializeField] private bool canPatrol = false;
        [ShowIf("@this.canPatrol"), SerializeField] private bool isPatrolling = false;
        [ShowIf("@this.canPatrol"), SerializeField] private List<Transform> patrolPoints;
        [ShowIf("@this.canPatrol"), SerializeField] private int currentPatrolPoint;

        
        private void Start()
        {
            sensing = GetComponentInChildren<AISensing>();
            agent = GetComponentInChildren<NavMeshAgent>();
        }

        private bool ReachedDestination(Vector3 pos1, bool ignoreY = true)
        {
            var position = agent.transform.position;
            return Vector3.Distance(position, pos1) < agent.radius + agent.stoppingDistance + Convert.ToInt32(ignoreY) * Mathf.Abs(position.y - pos1.y) || agent.pathStatus == NavMeshPathStatus.PathPartial && !agent.hasPath;
        }

        public void Chase(State state)
        {
            if (!isChasing)
            {
                Debug.Log("Chasing");
                isChasing = true;                
                chaseTarget = sensing.GetCurrentTarget();
                agent.speed *= chaseSpeed;
                if (!wasChasingBefore)
                {
                    chaseStartPosition = agent.transform.position;
                    wasChasingBefore = true;
                }
            }

            if (maxChaseDistance > 0 &&
                Vector3.Distance(agent.transform.position, chaseStartPosition) > maxChaseDistance)
            {
                Debug.Log("Chase distance passed");
                state.SetShouldExit(1);
            }

            if (!chaseTarget.Equals(sensing.GetCurrentTarget()))
            {
                Debug.Log("Target changed");
                state.SetShouldExit(1);
            }
            else
            {
                chaseTarget = sensing.GetCurrentTarget();
            }
            
            if (ReachedDestination(chaseTarget.GetLocation(), false))
            {
                if (chaseTarget.WasRemoved() || chaseTarget.GetData().GetInstance().GetSensorInput() == null)
                {
                    state.SetShouldExit(-3);
                }
                else
                {
                    state.SetShouldExit(-6);
                }
            }
            else if (ReachedDestination(chaseTarget.GetLocation()))
            {
                if (chaseTarget.WasRemoved() || chaseTarget.GetData().GetInstance().GetSensorInput() == null)
                {
                    state.SetShouldExit(-3);
                }
            }
            
            agent.SetDestination(chaseTarget.GetLocation());
        }

        public void Return(State state)
        {
            if (isChasing)
            {
                Debug.Log("Returning");
                isChasing = false;
                chaseTarget = null;
                agent.speed /= chaseSpeed;
                agent.SetDestination(chaseStartPosition);
            }
            
            if (ReachedDestination(chaseStartPosition))
            {
                wasChasingBefore = false;
                state.SetShouldExit(1);
            }
        }
        
        public void Apprehend(State state)
        {
            if (!(chaseTarget.GetData().GetInstance().GetSensorInput() == null) && !chaseTarget.WasRemoved() && chaseTarget.GetData().GetInstance().GetSensorInput().CompareTag("Player"))
            {
                Debug.Log("Criminal apprehended!");
                Time.timeScale = 0f;
                GameObject.Find("GameManager").GetComponent<GameManager>().LevelFailed();
            }
            else if (!(chaseTarget.GetData().GetInstance().GetSensorInput() == null) && !chaseTarget.WasRemoved())
            {
                Debug.Log("Investigating other source!");
                // Show investigate icon
                // Raise overall alert level if unnatural cause
            }
            else
            {
                state.SetShouldExit(1);
            }
        }
        
        private bool RandomPoint(Vector3 center, float range, out Vector3 result) {
            for (int i = 0; i < 30; i++) {
                Vector3 randomPoint = center + Random.insideUnitSphere * range;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, agent.height *  2, NavMesh.AllAreas)) {
                    result = hit.position;
                    return true;
                }
            }
            result = Vector3.zero;
            return false;
        }
        
        public void Search(State state)
        {           
            if (ReachedDestination(searchDestination) || !isSearching)
            {
                isSearching = true;            
                Debug.Log("Searching");
                Vector3 point;
                if (RandomPoint(transform.position, searchRadius, out point)) {
                    Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f);
                    searchDestination = point;
                    agent.SetDestination(searchDestination);
                }
            }
        }

        public void EndSearch(State state)
        {
            isSearching = false;
            agent.SetDestination(agent.transform.position);
        }
        
        public void Patrol(State state)
        {
            if (!isPatrolling)
            {
                isPatrolling = true;            
                Debug.Log("Patrolling");
            }

            if (ReachedDestination(patrolPoints[currentPatrolPoint].position))
            {
                currentPatrolPoint += 1;
                currentPatrolPoint %= patrolPoints.Count;
            }
            agent.SetDestination(patrolPoints[currentPatrolPoint].position);
        }

        public void EndPatrol(State state)
        {
            isPatrolling = false;
            agent.SetDestination(agent.transform.position);
        }
        
        private void OnDrawGizmos()
        {
            if (patrolPoints.Count > 0)
            {
                Gizmos.color = Color.black;
                for (int i = 0; i < patrolPoints.Count; i++)
                {
                    Transform patrolPoint = patrolPoints[i];
                    Gizmos.DrawCube(patrolPoint.position, new Vector3(0.25f, 0.25f, 0.25f));
                    Gizmos.DrawLine(patrolPoint.position, patrolPoints[(i + 1) % patrolPoints.Count].position);
                }
            }

            if (isChasing)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(chaseTarget.GetLocation(), new Vector3(0.25f, 0.25f, 0.25f));

            }
        }
    }
}