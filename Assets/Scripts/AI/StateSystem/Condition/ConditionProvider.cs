using System;
using UnityEngine;

namespace AI.StateSystem.Condition
{
    public class ConditionProvider : MonoBehaviour
    {
        [SerializeField] private AIStateMachine _stateMachine;
        [SerializeField] private AISensing _aiSensing;

        private void Start()
        {
            _stateMachine = GetComponentInChildren<AIStateMachine>();
            _aiSensing = GetComponentInChildren<AISensing>();
        }

        public void CheckCurrentTarget(Condition condition)
        {
            SensingInstance instance = _aiSensing.GetCurrentTarget();
            if (instance == null || instance.WasRemoved())
            {
                condition.SetSatisfied(false);
                return;
            }
            condition.SetSatisfied(CompareValues(condition.GetThreshold(),instance.GetCurrentPriority(), condition.GetComparator()));
        }
        
        public void CheckBestCandidate(Condition condition)
        {
            SensingInstance instance = _aiSensing.GetBestCandidate();
            if (instance == null || instance.WasRemoved())
            {
                condition.SetSatisfied(false);
                return;
            }
            condition.SetSatisfied(CompareValues(condition.GetThreshold(),instance.GetCurrentPriority(), condition.GetComparator()));
        }
        
        public void CheckStateDuration(Condition condition)
        {
            State currentState = _stateMachine.GetCurrentState();
            condition.SetSatisfied(CompareValues(condition.GetThreshold(), currentState.stateDuration, condition.GetComparator()));
        }
        
        public void CheckShouldExit(Condition condition)
        {
            State currentState = _stateMachine.GetCurrentState();
            condition.SetSatisfied(CompareValues(condition.GetThreshold(), currentState.GetShouldExit(), condition.GetComparator()));
        }


        private bool CompareValues(float threshold, float value, Comparator comparator)
        {
            switch (comparator)
            {
                case Comparator.LessEq:
                    return Math.Abs(threshold - value) < 0.02f || value < threshold;
                case Comparator.Less:
                    return Math.Abs(threshold - value) > 0.02f && value < threshold;
                case Comparator.Eq:
                    return Math.Abs(threshold - value) < 0.02f;
                case Comparator.GreaterEq:
                    return Math.Abs(threshold - value) < 0.02f || value > threshold;
                case Comparator.Greater:
                    return Math.Abs(threshold - value) > 0.02f && value > threshold;
            }

            Debug.Log("Inconceivable!");
            return false;
        }
    }
}