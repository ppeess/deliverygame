using System;
using UnityEngine;
using UnityEngine.Events;

namespace AI.StateSystem.Condition
{
    [Serializable]
    public class Condition
    {
        [SerializeField] private float threshold;
        [SerializeField] private Comparator comparator;
        public UnityEvent<Condition> getComparisonValue;
        [SerializeField] private bool isSatisfied = false;

        public bool IsSatisfied()
        {
            getComparisonValue.Invoke(this);
            return isSatisfied;
        }

        public void SetSatisfied(bool value)
        {
            isSatisfied = value;
        }
        
        public float GetThreshold()
        {
            return threshold;
        }
        
        public Comparator GetComparator()
        {
            return comparator;
        }
    }
}

public enum Comparator
{
    Greater,
    GreaterEq,
    Eq,
    LessEq,
    Less
}