using System;
using System.Collections.Generic;
using UnityEngine;

namespace AI.StateSystem
{
    [Serializable]
    public class Transition
    {
        [SerializeField] private string nextState;
        [SerializeField] private List<Condition.Condition> conditions;
        [SerializeField] private int priority = 1;

        public bool CanTransition()
        {
            foreach (Condition.Condition cond in conditions)
            {
                if (!cond.IsSatisfied())
                {
                    return false;
                }
            }
            return true;
        }

        public string GetNextState()
        {
            return nextState;
        }

        public int GetPriority()
        {
            return priority;
        }
    }
}