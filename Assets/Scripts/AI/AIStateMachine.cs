using System;
using System.Collections.Generic;
using AI.StateSystem;
using UnityEngine;

namespace AI
{
    public class AIStateMachine : MonoBehaviour
    {
        [SerializeField] private string initialState;
        [SerializeField] private State currentState;
        [SerializeField] private State nextState;
        [SerializeField] private List<State> states;
        [SerializeField] private bool switchingStates;

        public State GetCurrentState()
        {
            return currentState;
        }

        private void Start()
        {
            nextState = states.Find(state => state.identifier == initialState);
            Debug.Log(nextState);
            EnterState();
        }

        private void EnterState()
        {
            currentState = nextState;
            currentState.OnStateEnter();
            switchingStates = false;
        }

        private void Update()
        {
            currentState.DuringState(Time.deltaTime);
            if (!switchingStates)
            {
                string nextStateCandidate = currentState.CheckTransitions();
                if (nextStateCandidate != "")
                {
                    State stateCandidate = states.Find(state => state.identifier == nextStateCandidate);
                    nextState = stateCandidate;
                    currentState.OnStateExit();
                    switchingStates = true;
                }
            }
            else
            {
                if (currentState.GetExitStatus())
                {
                    EnterState();
                }
            }
        }
    }
}