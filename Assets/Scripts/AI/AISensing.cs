using System;
using System.Collections;
using System.Collections.Generic;
using AI.Sensors;
using UnityEngine;

namespace AI
{
    public class AISensing : MonoBehaviour
    {
        [SerializeField] private List<Sensor> sensors;

        [SerializeField] private float lingerDuration = 10f;
        [SerializeField] private float activeTargetLingerDuration = 30f;

        [SerializeField] private SensingInstance currentTarget = null;
        [SerializeField] private SensingInstance bestCurrentCandidate = null;
        [SerializeField] private List<SensingInstance> possibleTargets;

        public SensingInstance GetCurrentTarget()
        {
            return currentTarget;
        }
        
        public SensingInstance GetBestCandidate()
        {
            return bestCurrentCandidate;
        }

        private void Start()
        {
            sensors = new List<Sensor>(GetComponentsInChildren<Sensor>());
            possibleTargets ??= new List<SensingInstance>();
            foreach (Sensor sensor in sensors)
            {
                sensor.AddListener(AddInput);
                sensor.AddLostListener(RemoveInput);
            }
        }

        private void Update()
        {
            SelectCandidate();
        }

        private void AddInput(SensorInputInstanceData instance, float value)
        {
            SensingInstance sensingInstance = new SensingInstance(instance, value);
            if (!possibleTargets.Contains(sensingInstance))
            {
                possibleTargets.Add(sensingInstance);
            }
            else
            {
                int targetIndex = possibleTargets.FindIndex(instance1 => instance1.Equals(sensingInstance));
                if (possibleTargets[targetIndex].WasRemoved())
                {
                    possibleTargets[targetIndex].SetRemoved(false);
                    possibleTargets[targetIndex].SetData(instance);
                }
                possibleTargets[targetIndex].SetDistanceValue(value);
            }
        }
        
        private void RemoveInput(SensorInputInstanceData instance)
        {
            SensingInstance sensingInstance = possibleTargets.Find(instance1 => instance1.Equals(new SensingInstance(instance, 0f)));
            sensingInstance.SetRemoved(true);
            StartCoroutine(RemoveTarget(sensingInstance));
        }

        private void SelectCandidate()
        {
            if (possibleTargets.Count > 0)
            {
                foreach (SensingInstance sensingInstance in possibleTargets)
                {
                    sensingInstance.Update(Time.deltaTime);
                    sensingInstance.GetCurrentPriority();
                }
                possibleTargets.Sort(CompareCandidates);
                currentTarget = possibleTargets[0];
                bestCurrentCandidate = SelectFirstNotNull(possibleTargets);
            }
            else
            {
                currentTarget = null;
                bestCurrentCandidate = null;
            }
        }

        private SensingInstance SelectFirstNotNull(List<SensingInstance> instances)
        {
            foreach (SensingInstance sensingInstance in instances)
            {
                if (!sensingInstance.WasRemoved())
                {
                    return sensingInstance;
                }
            }

            return instances[0];
        }

        private int CompareCandidates(SensingInstance input1, SensingInstance input2)
        {
            float value = input1.GetCurrentPriority() - input2.GetCurrentPriority();
            if (Mathf.Abs(value) > 0.02)
            {
                if (value > 0)
                {
                    return -1;
                }
                return 1;
            }
            else
            {
                return 0;
            }
        }

        private void OnDestroy()
        {
            foreach (Sensor sensor in sensors)
            {
                sensor.RemoveListener(AddInput);                
                sensor.RemoveLostListener(RemoveInput);
            }
        }

        private IEnumerator RemoveTarget(SensingInstance removedSensingInstance)
        {
            float remainingTime = lingerDuration;
            if (removedSensingInstance.Equals(currentTarget))
            {
                remainingTime = activeTargetLingerDuration;
            }

            while (remainingTime > 0)
            {
                if (removedSensingInstance.WasRemoved())
                {
                    remainingTime -= Time.deltaTime;
                    yield return null;
                }
                else
                {
                    yield break;
                }
            }
            
            possibleTargets.Remove(removedSensingInstance);
            if (removedSensingInstance.Equals(currentTarget))
            {
                SelectCandidate();
            }
        }
    }
}